<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Customers;
use Illuminate\Http\Request;
use Validator;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('form');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required','unique:users',
            'password' => 'required',
            'first_name' => 'required|max:30',
            'last_name' => 'required|max:30',
            'call_name' => 'required|max:30',
            'no_ktp' => 'required',
            'no_npwp' => 'required',
        ]);

        $user = new User();
        $customer = new Customers();

        if (!$validator->passes()) {
            $output = [
                'message' => $validator->errors()->all()
            ];
            return response()->json($output, 422);
        }


        // dd($request);
        $datauser=[
            'email'=>$request->email,
            'password'=>bcrypt($request->password)
        ];
        $dataCustomer=[
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'call_name'=>$request->call_name,
            'no_ktp'=>$request->no_ktp,
            'no_npwp'=>$request->no_npwp,
            'contact_no'=>$request->contact_no,

        ];

        try {
            //code...
            DB::beginTransaction();
            $IDUser=$user->insertData($datauser);
            $dataCustomer['user_id']=$IDUser;
            $customer->insertData($dataCustomer);
            DB::commit();
            // dd($IDUser);

        } catch (\Throwable $th) {
            DB::rollback();
            // dd($th);
            throw $th;
            
        }
       
        $output = [
            'message' => 'Success',
            'error' => $validator->errors()->all()
        ];

        return redirect('/form');
    }

}
