<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model

{
    protected $primaryKey = 'customer_id';
    protected $table = 'customers';
    protected $fillable = [
        'first_name', 
        'last_name', 
        'call_name', 
        'no_ktp', 
        'no_npwp', 
        'contact_no'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    function insertData($array){
        Customers::insert($array);
    }
}
