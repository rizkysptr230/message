<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <style media="screen">
        .register {
            height: 100vh;
            align-items: center;
            justify-content: center;
        }
        .card {
            padding: 48px;
            border-radius: 16px;
            box-shadow: 0 0 14px 0 rgba(0, 0, 0, 0.175);
            border: none;
        }
        .btn {
            border-radius: 8px;
            font-weight: 500;
        }
        .btn-primary:not(:disabled):not(.disabled):active:focus.btn-primary:not(:disabled):not(.disabled):active:focus {
            box-shadow: none;
        }
        .btn:disabled {
            background-color: #e6e7e8 !important;
            color: #aaa !important;
            border: none !important;
        }
        .form-control {
            border-radius: 8px;
            outline: none !important;
        }
        .form-control:focus {
            box-shadow: none;
            border-color: #007bff;
        }
        .form-group {
            margin-bottom: 24px;
        }
        .form-group:last-child {
            margin-bottom: 0;
        }
    </style>
    <div class="register">
        <div class="col-lg-4 col-10 mx-auto">
            <div class="card">
                <!-- <div class="alert alert-danger error-messages" style="display:none">
                    <ul></ul>
                </div> -->

                <form id="userForm" method="post">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" type="text" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" id="password" name="password">
                    </div>
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input class="form-control" type="text" id="first_name" name="first_name">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input class="form-control" type="text" id="last_name" name="last_name">
                    </div>
                    <div class="form-group">
                        <label for="call_name">Call Name</label>
                        <input class="form-control" type="text" id="call_name" name="call_name">
                    </div>
                    <div class="form-group">
                        <label for="no_ktp">Number KTP</label>
                        <input class="form-control" type="text" id="no_ktp" name="no_ktp">
                    </div>
                        <div class="form-group">
                            <label for="no_npwp">Number NPWP</label>
                            <input class="form-control" type="text" id="no_npwp" name="no_npwp">
                        </div>
                        <div class="form-group">
                            <label for="contact_no">Contact Number</label>
                            <input class="form-control" type="text" id="contact_no" name="contact_no">
                        </div>
                    <div class="form-group">
                        {{ csrf_field() }}
                        <button class="btn btn-primary btn-block" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>

    </script>
</body>
</html>
